// 1.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json))


// 2.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json.map((item) => {
	return item.title;})
));

// 3.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log("The item \" " +json.title + " \" on the list has a status of " +json.completed));



// 4.
fetch('https://jsonplaceholder.typicode.com/todos/',
{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		userId: 1,
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// 5.
fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated To Do List Item',
		userId: 1,
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// 6.
fetch('https://jsonplaceholder.typicode.com/todos/1',
{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'delectus aut autem',
		status: "Complete",
		dateCompleted: "03/30/23",
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// 7.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
})
